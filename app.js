var express = require('express');
var osmosis = require('osmosis');
var app = express();

var config = require('./config.json');

function sendSMS(res){
    osmosis.get('http://www.magtifun.ge/index.php')
        .login(config.username,config.password)
        .then(function (context,data) {})
        .find('body')
        .set('resp')
        .delay(2)
        .get('index.php?page=2&lang=ge')
        .then(function (context,data) {
            try {
                context.getElementById("sms_form").setAttribute("action", "scripts/sms_send.php");
            } catch (err) {
                res.send('პაროლი ან მომხმარებელი არაზუსტია!')
            }
        })
        .submit("#sms_form",{
            recipients: config.recipient,
            message_body: config.message,
            total_recipients: 0,
            messages_count: 1,
            message_unicode: 0
        })
        .then(function (context,data) {})
        .find('body')
        .set('resp2')
        .data(function(result) {
            if(result.resp.includes("არაზუსტია")){
                res.send('პაროლი ან მომხმარებელი არაზუსტია!')
            }else{
                res.send('OK')
            }
        });
}

app.get('/', function (req, res) {
    sendSMS(res);
});

app.listen(3000, function () {
    console.log("listening");
});